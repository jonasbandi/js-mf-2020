//
// // For a comprehensive discussion see:
// // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
//
//
//
(function(){
    function fun() { return this; } // `this` is the global context

    var value = fun();
    console.log('`this` is the global context:' + (value === global)); // use `global` in nod or `window` in the browser
})();

(function(){
    "use strict";

    function fun() { return this; } // in strict mode `this` is undefined

    var value = fun();
    console.log('`this` is undefined: ' + (value === undefined));

})();




(function(){
    leaking1 = 42; // undeclared variables are leaking on the global scope
})();
console.log('Global veriable: ' + global.leaking1); // use `global` in nod or `window` in the browser

(function(){
    function inner(){
        "use strict";
        leaking2 = 42; // syntax error prevents leaking of undeclared variables
    }
    inner();
})();