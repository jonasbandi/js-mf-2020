# SimpleToDo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.8.

To run the project:

	npm install
	npm start
	
The url to the REST-API	 is hardcoded in `src/app/todos/model/todo.service.ts`.  
Make sure the REST-API is running!
