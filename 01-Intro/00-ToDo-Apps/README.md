# Starting the apps:

    npm start
    
Then go to:


# Deploying to now

    npm run deploy


#  Building

    npm run build
    
This builds the two SPA projects (`02-ToDo-SPA` and `02-ToDo-SPA-API`) and then copies the static assets to `01-ToDo-Server/public`
