// TODO: Separate layout and code
// TODO: Include 3rd party functionality

const addToDo = () => {
  console.log('Button', this);
  var input = document.getElementById('todo-text');
  var node = document.createElement('li');
  var textnode = document.createTextNode(input.value);
  node.appendChild(textnode);
  document.getElementById('todo-list').appendChild(node);
  input.value = '';
};

var addButton = document.getElementById('add-button');
addButton.addEventListener('click', addToDo);
