
## Exercise 1: Language Constructs

In this exercise you get to know certain JavaScript language constructs by completing "automated lessons":

- Change into the directory `02-Language/90-Lessons`
- Start a web server in the directory and open `index.html` in the browser. Either use `lite-server` (installed globally via npm) on the commandline or open the file from WebStorm by choosing 'Open in Browser'.
- You will see some failing tests.
- Open the corresponding JavaScript file in an editor. i.e: `00-CourseSelection.js`. Read the code of corresponding lesson. 
- Edit the code, so that the first Tests should be passing.
- Refresh the browser. The test should turn green.
- Advance to the next test.



## Exercise 2: Capturing A Context with a Closure

Inspect the code in the example `03-Patterns/05-CapturingScope/index.html`. Click on the numbers and the Button: Did you expect the given behaviour?  

Change the code, so that:

- when you click on a number then that number is displayed in the alert dialog 
- clicking the button shows an increasing number in the alert

Use closures to achieve that effect.



## Exercise 3: From Spaghetti to Modules

Inspect the example `03-Patterns/03-Spaghetti`: Include the new functionality on the page by un-commenting the commented code in `index.html`.  
Why is there a side-effect form the newly included functionality on the existing functionality?

Change the example, so that there are no side-effects between the existing and the newly included functionality.

Use IIFEs/closures to wrap each functionality in a module that is completely isolated.



## Exercise 4: TypeScript from Scratch

Install the TypeScript compiler, write a TypeScript class and inspect the transpiled result:

- Start a new npm project: `npm init`
- Install TypeScript `npm i -D typescript`
- Create the TypeScript configuration: `npx tsc --init` 
  (or use WebStorm *Create New -> tsconfic.json* and adjust it.
- Create a npm task that transpiles TypeScript to JavaScript with `tsc` 
- Write a ES2015 class and annotate it with type information
- Inspect the transpiled output




## Exercise 5: TypeScript with 3rd Party Libraries

In the example `10-TypeScript/91-3rdParty`:

- Run the example:

```
	npm install
	npm start
```

- A browser should open and you see an error at runtime.

- Rename the file `app.js` to `app.ts` ... do you see any changes?

- Install the type definitions for `lodash`:

```
npm install -D @types/lodash
```

- What did change? You should now get a compile error ...




