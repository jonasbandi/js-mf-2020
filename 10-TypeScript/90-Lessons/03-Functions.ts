lesson("about arrow functions", () => {

    learn("different ways to declare arrow functions", () => {

        let square = x => x * x; // just one argument
        let add = (a, b) => a + b;
        let pi = () => 3.1415; // no arguments

        let doMore = x => {
            console.log('HELLO!');
            return x + 1;
        };

        let doStuff = x => {
            console.log('HELLO!');
            console.log(x);
        };

        expect(square(5)).toBe(FILL_ME_IN);
        expect(add(2, 3)).toBe(FILL_ME_IN);
        expect(pi()).toBe(FILL_ME_IN);
        expect(doMore(41)).toBe(FILL_ME_IN);
        expect(doStuff(41)).toBe(FILL_ME_IN);
    });

    learn("that 'this' is dynamically bound for traditional function", done => {

        class Controller {
            counter = 0;
            increment(){
                this.counter++;
            }
        }
        const controller = new Controller();

        setTimeout(controller.increment, 1);

        setTimeout(() => {
            expect(controller.counter).toBe(FILL_ME_IN);
            done();
        }, 2)
    });

    learn("that 'this' is lexically bound for arrow function", done => {

        class Controller {
            counter = 0;
            increment = () => this.counter++;
        }
        const controller = new Controller();

        setTimeout(controller.increment, 1);

        setTimeout(() => {
            expect(controller.counter).toBe(FILL_ME_IN);
            done();
        }, 2)
    });

    learn("that 'this' is dynamically bound in ES5", () => {

        let rob = {
            _name: "Bob",
            _friends: ['John'],
            printFriends() {
                var out;

                // Note: Array.forEach -> the second parameter is bound the 'this' in the function passed as first parameter
                // See: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach
                this._friends.forEach(function (f) {
                    out = this._name + ' knows ' + f;
                }, {_name: 'Alice'});

                return out;
            }
        };

        expect(rob.printFriends()).toBe(FILL_ME_IN);
    });


    learn("that 'this' is not bound explicitly inside an arrow functions", () => {

        let rob = {
            _name: "Bob",
            _friends: ['John'],
            printFriends() {
                let out = '';
                this._friends.forEach(f => out += this._name + ' knows ' + f + '!', {_name: 'Alice'});

                return out;
            }
        };

        expect(rob.printFriends()).toBe(FILL_ME_IN);
    });

    learn("that 'this' is just resolved up the scope chain with arrow functions", () => {

        function foo() {
            return () => {
                return () => {
                    return () => {
                        return this.id;
                    };
                };
            };
        }

        expect(foo.call( { id: 42 } )()()()).toBe(FILL_ME_IN);
    });

    learn("that 'this' can be bound explicitly with regular functions", () => {

        const greeter = {
            message: 'from object',
            greet: function () {
                return 'Hello ' + this.message + '!';
            }
        };

        // Note: 'this' is the global context (window) here. We are not in strict mode
        this.message = 'from lexical context';

        const func1 = greeter.greet;
        const func2 = greeter.greet.bind(greeter);

        expect(func1()).toBe(FILL_ME_IN);
        expect(func2()).toBe(FILL_ME_IN);
    });


    learn("that 'this' can't be bound explicitly in arrow functions", () => {

        const greeter = {
            message: 'from object',
            greet: () => 'Hello ' + this.message + '!'
        };

        // Note: 'this' is the global context (window) here. We are not in strict mode
        this.message = 'from lexical context';

        const func1 = greeter.greet;
        const func2 = greeter.greet.bind(greeter);

        expect(func1()).toBe(FILL_ME_IN);
        expect(func2()).toBe(FILL_ME_IN);
    });

});

