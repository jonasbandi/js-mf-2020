Install project dependencies:

    npm install

Run development build:

    npm start
    
-> Then go to `http://localhost:8181`

Build a production build:

    npm run build
    
Serve a production build:

    npm run serve
    
