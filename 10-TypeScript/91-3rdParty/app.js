const champions = [
    {name: 'Katniss'},
    {name: 'Peeta'},
    {name: 'Johanna'},
    {name: 'Haymitch'},
    {name: 'Konrad'}
];

const characters  = _.filter(champions, c => c.startsWith('K'));
const out = characters.join(' ');

document.write(out);



// DEMO
// - rename js -> ts
//
// - add type declaration:
//      declare var _: { filter: (collection: any[], predicate: (e: any) => boolean) => any[]}
// - even better:
//             declare const _: {
//                 filter<T>(a: T[], p: (e: T) => boolean): T[]
//             };
// - alternative:
//             declare var _: { filter: <T>(collection: T[], predicate: (e: T) => boolean) => T[]}
//
// Finally: Install types from npm
// - npm i -D @types/lodash
