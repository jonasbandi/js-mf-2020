{
    class Person {
        private name: string = 'Tyler';
        age: number = 42;

        getInfo() {
            console.log(this.name); // inspect `this`
            return this.name + this.age;
        }
    }

    const p = new Person();
    console.log(p.getInfo());
    // console.log(p.name);

    // TODO:
    // - add access control
    // - add constructor
    // - add property shortcuts

    class Employee extends Person {
        company: string = 'Paper Mill';

        getInfo() {
            return super.getInfo() + this.company;
        }
    }

    const e = new Employee();
    console.log(e.getInfo());


    // `this`-binding roblems are not solved by typescript!
    setTimeout(p.getInfo, 1);


    function doSomething(person: Person) {
        console.log(person.getInfo());
    }

    doSomething(e);
    doSomething(p);
}