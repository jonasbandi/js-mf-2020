{
    interface IBird {
        fly();
        layEggs();
    }

    interface IFish {
        swim();
        layEggs();
    }

    class Fish implements IFish {
        swim() {
        }

        layEggs() {
        }
    }

    // Intersection Type
    let flyingFish: IFish & IBird = {
        swim(){
        }, fly(){
        }, layEggs(){
        }
    };
    flyingFish.swim();

    let pet: IBird | IFish = {
        fly(){
        }, layEggs(){
        }
    };
    pet = new Fish();
    pet.layEggs(); // okay
    // pet.swim() // Error

    // Using Type Guards
    (pet as IFish).swim();
    (<IFish>pet).swim();

    if (pet instanceof Fish) {
        pet.swim();
    }
}

