
// *** Using ES2015 Promises ***
var promises = [];
for (var i = 0; i < 9; i++) {
    promises.push($.get('http://localhost:3456/word/' + i));
}

var promise = Promise.all(promises);
promise.then(function (data) {
    document.getElementById('content').textContent = data.join(" ");
});


// *** Using jQuery only ***
// var promises = [];
// for (var i = 0; i < 9; i++) {
//     promises.push($.get('http://localhost:3456/word/' + i));
// }
// var finalPromise = $.when.apply($, promises);
// finalPromise.then(function () {
//     var data = Array.prototype.map.call(arguments, r => r[0]);
//     document.getElementById('content').textContent = data.join(" ");
// });


// *** Using jQuery but ES2015 rest/spread syntax ***
// var promises = [];
// for (var i = 0; i < 9; i++) {
//     promises.push($.get('http://localhost:3456/word/' + i));
// }
// var finalPromise = $.when(...promises);
// finalPromise.then(function (...data) {
//     document.getElementById('content').textContent = data.map(r => r[0]).join(" ");
// });


// *** Failing attempt at chaining ***
// var words = [];
// for (var i = 0; i < 9; i++) {
//     var promise = $.get('http://localhost:3456/word/' + i);
//     promise.then((function (index) {
//         return function (data) {
//             words[index] = data;
//             updateUi();
//         }
//     })(i));
// }
//
// function updateUi() {
//     document.getElementById('content').textContent = words.join(" ");
// }


// *** Chaining ***
// $.get('http://localhost:3456/word/0')
//     .then(function(data){ appendWord(data); return $.get('http://localhost:3456/word/1')})
//     .then(function(data){ appendWord(data); return $.get('http://localhost:3456/word/2')})
//     .then(function(data){ appendWord(data); return $.get('http://localhost:3456/word/3')})
//     .then(function(data){ appendWord(data); return $.get('http://localhost:3456/word/4')})
//     .then(function(data){ appendWord(data); return $.get('http://localhost:3456/word/5')})
//     .then(function(data){ appendWord(data); return $.get('http://localhost:3456/word/6')})
//     .then(function(data){ appendWord(data); return $.get('http://localhost:3456/word/7')})
//     .then(function(data){ appendWord(data); return $.get('http://localhost:3456/word/8')})
//     .then(function(data){ appendWord(data); });
//
// function appendWord(word){
//    document.getElementById('content').textContent += ' ' + word;
// }

// *** Async/Await ***
// async function getData(){
//
//   for (var i = 0; i < 9; i++) {
//     let data = await $.get('http://localhost:3456/word/' + i);
//     appendWord(data);
//   }
// }
// function appendWord(word){
//   document.getElementById('content').textContent += ' ' + word;
// }
// getData();

// *** Chaining with loop ***
// var promise = $.get('http://localhost:3456/word/0');
// for (var i = 1; i <= 9; i++) {
//     promise = promise.then((function (index) {
//         return function (data) {
//             document.getElementById('content').textContent += ' ' + data;
//             return $.get('http://localhost:3456/word/' + index);
//         };
//     }(i)));
// }


// *** Recursion ***
// getNextWord({number: 0});
//
// function getNextWord(command){
//
//    if (command.word) {
//        document.getElementById('content').textContent += ' ' + command.word;
//    }
//    $.get('http://localhost:3456/word/' + command.number)
//            .then(function(data){
//                if (data){
//                    var newCommand = {word:data, number: command.number + 1};
//                    getNextWord(newCommand);
//                }
//            });
// }
