const getBtn = document.getElementById('get');
const postBtn = document.getElementById('post');
const idInput = document.getElementById('id');
const putBtn = document.getElementById('put');
const deleteBtn = document.getElementById('delete');

getBtn.addEventListener('click', getData);
postBtn.addEventListener('click', postData);
putBtn.addEventListener('click', putData);
deleteBtn.addEventListener('click', deleteData);


function getData() {

  $.get('http://localhost:8855/comments', function (data) {
    console.log(data);
  });
}

function postData() {

  // // Note: jQuery does a form post which does cause need a preflight request
  // $.post('http://localhost:3001/comments', {text: 'test - ' + new Date()} , function () {
  //     console.log('POST!');
  // });

  // Note: Posting json causes a preflight request
  $.ajax({
    url: 'http://localhost:8855/comments',
    type: 'POST',
    data: JSON.stringify({text: 'test - ' + new Date()}),
    contentType: "application/json",
    success: function () {
      console.log('POST!')
    }
  });
}

function putData() {
  var id = idInput.value;

  $.ajax({
    url: 'http://localhost:8855/comments/' + id,
    type: 'PUT',
    data: {text: 'test - ' + new Date()},
    success: function () {
      console.log('PUT!')
    }
  });
}

function deleteData() {
  var id = idInput.value;

  $.ajax({
    url: 'http://localhost:8855/comments/' + id,
    type: 'DELETE',
    success: function () {
      console.log('DELETED!')
    }
  });
}
