var gulp = require('gulp');
var Builder = require('systemjs-builder');


gulp.task('bundle', function() {
    var builder = new Builder();
    return builder.loadConfig('./app/system.config-build.js')
        .then(function() {
            return builder.buildStatic('app/src/app', 'build/bundle.js', {minify: true, mangle: false, rollup: true})
        })
});