import $ from 'jquery';

export let todoListView = {
    init() {
        this.todoCountElem = $('#total');
        this.todoListElem = $('#do')

        let $todoListView = $(this);
        this.todoListElem.on('click', '.remove', (e) => {
            let index = $(e.target).parents('li').data('index');
            $todoListView.trigger('item-removed', index);
        });
    },
    render(todos) {
        this.todoCountElem.text(todos.length);

        this.todoListElem.html('');
        let i = 0;
        for (let todo of todos) {
            this.todoListElem.append(
                '<li class="clearfix" data-index="' + i + '">' +
                todo.text +
                '<span class="pull-right">' +
                '<button class="btn btn-xs btn-danger remove glyphicon glyphicon-trash"></button>' +
                '</span>' +
                '</li>'
            );
            i++;
        }
    }
};
