module.exports = {
    entry: './src/app.js',
    output: {
        filename: 'build/bundle.js'
    },


    module: {
        loaders: [
            {test: /\.js$/, loader: 'babel', exclude: /node_modules/}
        ]
    }
};