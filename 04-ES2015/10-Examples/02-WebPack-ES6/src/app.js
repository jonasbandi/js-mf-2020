import $ from 'jquery';
import {add, square, pi} from './lib/math';

$('<div>').text(add(41,4)).appendTo(document.body);
$('<div>').text(square(4)).appendTo(document.body);
$('<div>').text(pi).appendTo(document.body);

