Install webpack globally:
    
    npm install -g webpack 
    
Install project dependencies:

    npm install
    
   
Run the webpack build:

    webpack //for building once for development
    webpack -p //for building once for production (minification)
    webpack --watch //for continuous incremental build in development (fast!)
    webpack -d //to include source maps


