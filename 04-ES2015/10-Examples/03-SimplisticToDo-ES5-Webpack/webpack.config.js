var config = {
	devtool: 'source-map',
	entry:'./app/src/app.js',
	output: {
		path: './app',
		filename: 'dist/bundle.js'
	},
	module: {
		loaders: [
			{test: /\.js$/, loader: 'babel', exclude: /node_modules/}
		]
	},
	devServer: {
        contentBase: "./app",
	}
	
};

module.exports = config;