var angular = require('angular');
var angularUiBootstrap = require('angular-ui-bootstrap');

var courseRater = require('./course-rater/courseRater');
var ratingList = require('./rating-list/ratingList');
var tableCounter = require('./table-counter/tableCounter');

var ratingApp = angular.module('RatingApp', [angularUiBootstrap]);

ratingApp.directive('courseRater', courseRater);
ratingApp.directive('ratingList', ratingList);
ratingApp.directive('tableCounter', tableCounter);

