
module.exports = function () {
    return {
        restrict: 'E',
        template: require('./course-rater.html'),
        controller: Controller,
        controllerAs: 'vm'
    }
};

function Controller($uibModal, $log) {
    var vm = this;
    vm.ratings = [
        {name: 'Will Hunting', rating: 3.50, entered: "2013-08-05"}
    ];

    vm.addRating = function () {
        vm.ratings.push({name: vm.name, rating: vm.rating, entered: new Date()});
    };

    vm.removeRating = function (item) {

        var modalInstance = $uibModal.open({
            template: require('./modal.html'),
            controller: ModalCtrl,
            controllerAs: 'vm'
        });

        modalInstance.result.then(function () {
            vm.ratings.splice(vm.ratings.indexOf(item), 1);
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });

    }
}

function ModalCtrl($uibModalInstance) {
    var vm = this;

    vm.ok = function () {
        $uibModalInstance.close();
    };

    vm.cancel = function () {
        $uibModalInstance.dismiss();
    };
}
