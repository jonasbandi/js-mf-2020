// ES5
var x = 'hi!';

if(true){
    var x = 'bye!';
}

console.log(x);


// ES2015
let y = 'hi!';
const z = 'hi!'

if(true){
    let y = 'bye!';
    const z = 'bye!'
    //z = 'no!' // not allowed
}

//let x = 'no!'; // not allowed

console.log(y);
console.log(z);



// In ES2015 declarations are still hoisted,
// but access is not allowed until the variable is initialized (temporal dead zone)
// See: https://rainsoft.io/variables-lifecycle-and-why-let-is-not-hoisted/

let foo = () => bar; // Correct: bar is not yet accessed
// console.log(bar); // -> ERROR: bar is not initialized
let bar = 'bar';
foo(); // Correct: when bar is accesses it is initialized