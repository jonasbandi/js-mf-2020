var firstName = 'Jonas';
var lastName = 'Bandi';
var greet = function(){console.log('Hello!');}

//ES5
var o1 = {
    firstName: firstName,
    lastName: lastName,
    greet: greet
};

console.log(o1);

//ES6: property names can be omitted
var o2 = {
    firstName,
    lastName,
    greet
};

console.log(o2);
o2.greet();

var color = 'red';
var o3 = { o2, color };

console.log(o3);


var o4 = {
    firstName,
    lastName,
    say(){console.log('Bye!');} // shortcut notation for object methods
};

o4.say();

var prop = 'say'
var o5 = {
    firstName,
    lastName,
    [prop]: function(){console.log('Meta!');} // dynamic properties
};

o5.say();
