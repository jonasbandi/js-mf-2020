let person = {name: 'John Smith'};
let tpl1 = `My name is ${person.name}.`;

let tpl2 = `
    My
        name
            is
                ${person.name}
`;

let tpl3 = `My name is ${person.name + new Date()}.`;

console.log(tpl1);
console.log(tpl2);
console.log(tpl3);



function myTag(strings, ...expressions){
    const part1 = strings[0];
    const part2 = strings[1];
    const part3 = strings[2];
    return part3 + expressions[0] + part2 + expressions[1] + part1  ;
}

const exp1 = 'E1';
const exp2 = 'E2';
const output = myTag`Part 1 ${exp1} Part 2 ${exp2} Part 3`;

console.log(output);