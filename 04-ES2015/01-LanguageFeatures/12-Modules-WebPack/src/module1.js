export class Greeter {

  sayHello(){
    console.log('Hi from module 1!');
  }
}

export function greet() {
  console.log('Hello from module 1');
}