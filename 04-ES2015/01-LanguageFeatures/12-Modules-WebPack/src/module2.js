export class Greeter {
  greet() {
    console.log('Howdy from module 2');
  }
}

export function greet() {
  console.log('Greetings from module 2');
}