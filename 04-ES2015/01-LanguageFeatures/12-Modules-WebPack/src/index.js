import {Greeter} from './module1';


console.log('Loading app ...');

const greeter1 = new Greeter();
greeter1.sayHello();


document.getElementById('loader')
  .addEventListener('click', async () => {

    const {Greeter} = await import(/* webpackChunkName: 'module2' */ './module2');
    const greeter2 = new Greeter();
    greeter2.greet();

  });


